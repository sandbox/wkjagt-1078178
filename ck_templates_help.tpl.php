<h3><?php print t('Background'); ?></h3>
<p>
    <?php print t('The CK Editor is a very nice wysiwyg editor that has been integrated in Drupal with the module found at
    <a href="http://drupal.org/project/ckeditor" title="http://drupal.org/project/ckeditor" rel="nofollow">http://drupal.org/project/ckeditor</a>.
    An interesting feature of the CK Editor is the use of templates. A template consists of predefined HTML
    that can be used as a starting point for newly created nodes.'); ?>
</p>

<h3><?php print t('The problem'); ?></h3>
<p>
    <?php print t('One drawback of this system is the way these templates are created: the templates are strings in a javascript file.
    Two disadvantages of this are that it\'s a very user-unfriendly way of entering HTML, and when a template is created
    modified, the updated code needs to be deployed to your production environment.'); ?>
</p>

<h3><?php print t('The solution'); ?></h3>
<p>
    <?php print t('This module uses Drupal\'s nodes to create templates. It defines its own node type that will only be used for
    creating templates, and that will be hidden from public view (as it is not real content). Adding a new template to
    the CK editor is now as simple as creating it in the Drupal interface. It will be immediately available as a
    template in the CK editor.'); ?>
</p>

<h3><?php print t('Installation'); ?></h3>
<ol>
    <li><?php print t('Install CK Editor (instructions on <a href="http://drupal.org/project/ckeditor" title="http://drupal.org/project/ckeditor" rel="nofollow">http://drupal.org/project/ckeditor</a>)'); ?></li>
    <li><?php print t('In the CK Editor settings, make sure you select a toolbar that includes the templates button'); ?></li>
    <li><?php print t('Install the CK Templates module from the current page'); ?></li>
    <li><?php print t('Open the ckeditor.config.js configuration file in your CK Editor installation and add the following line inside the CKEDITOR.editorConfig function: <pre>config.templates_files = [ \'/ck_templates.js\'];</pre>'); ?></li>
</ol>

<h3><?php print t('Coloured HTML syntax (optional)'); ?></h3>
<p>
    <?php print t('In your CK Templates installation you will find an almost empty codemirror directory. Codemirror is an awesome
    javascript library developed by Marijn Haverbeke for adding coloured code and automatic indentation to textareas.
    By downloading the library and placing it in the codemirror directory inside your CK Templates directory, you will
    get these functionalities while creating / modifying templates for the CK Editor.'); ?>
</p>
