CKEDITOR.addTemplates( 'default',
{
	// The name of sub folder which hold the shortcut preview images of the templates.
	imagesPath : CKEDITOR.getUrl( CKEDITOR.plugins.getPath( 'templates' ) + 'templates/images/' ),

	// The templates definitions.
	templates :
		[
<?php foreach ($nodes as $node) : ?>
			{
				title: '<?php print check_plain($node->title); ?>',
				html:  '<?php print $node->body; ?>'
			},
<?php endforeach; ?>
		]
});
