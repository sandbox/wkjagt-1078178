var editor = CodeMirror.fromTextArea('edit-body', {
  height             : "350px",
  parserfile         : "parsexml.js",
  stylesheet         : "/" + Drupal.settings.codemirror_path + "/css/xmlcolors.css",
  path               : "/" + Drupal.settings.codemirror_path + "/js/",
  continuousScanning : 500,
  lineNumbers        : true,
  indentUnit         : 4
});